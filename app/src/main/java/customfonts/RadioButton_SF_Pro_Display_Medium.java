package
        customfonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

import androidx.core.content.res.ResourcesCompat;

import com.lapak.driverlapakkite.R;

@SuppressLint("AppCompatCustomView")
public
class
RadioButton_SF_Pro_Display_Medium
extends
RadioButton
{




public
RadioButton_SF_Pro_Display_Medium(Context
context,
AttributeSet
attrs,
int
defStyle)
{








super(context,
attrs,
defStyle);








init();




}





public
RadioButton_SF_Pro_Display_Medium(Context
context,
AttributeSet
attrs)
{








super(context,
attrs);








init();




}





public
RadioButton_SF_Pro_Display_Medium(Context
context)
{








super(context);








init();




}





private
void
init()
{








if
(!isInEditMode())
{












setTypeface(ResourcesCompat.getFont(getContext(), R.font.montserrat_bold));








}




}
}
