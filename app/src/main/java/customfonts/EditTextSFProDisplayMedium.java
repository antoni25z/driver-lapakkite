package
        customfonts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.lapak.driverlapakkite.R;

public
class
EditTextSFProDisplayMedium
extends
androidx.appcompat.widget.AppCompatEditText
{




public
EditTextSFProDisplayMedium(Context
context,
AttributeSet
attrs,
int
defStyle)
{








super(context,
attrs,
defStyle);








init();




}





public
EditTextSFProDisplayMedium(Context
context,
AttributeSet
attrs)
{








super(context,
attrs);








init();




}





public
EditTextSFProDisplayMedium(Context
context)
{








super(context);








init();




}





private
void
init()
{








if
(!isInEditMode())
{












setTypeface(ResourcesCompat.getFont(getContext(), R.font.montserrat_bold));








}




}
}
