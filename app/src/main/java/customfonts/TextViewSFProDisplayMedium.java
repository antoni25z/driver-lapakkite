package
        customfonts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.lapak.driverlapakkite.R;

public
class
TextViewSFProDisplayMedium
extends
androidx.appcompat.widget.AppCompatTextView
{




public
TextViewSFProDisplayMedium(Context
context,
AttributeSet
attrs,
int
defStyle)
{








super(context,
attrs,
defStyle);








init();




}





public
TextViewSFProDisplayMedium(Context
context,
AttributeSet
attrs)
{








super(context,
attrs);








init();




}





public
TextViewSFProDisplayMedium(Context
context)
{








super(context);








init();




}





private
void
init()
{








if
(!isInEditMode())
{












setTypeface(ResourcesCompat.getFont(getContext(), R.font.montserrat_regular));








}




}
}
