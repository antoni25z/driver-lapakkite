package
        customfonts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.lapak.driverlapakkite.R;

public
class
EditTextSFProDisplayRegular
extends
androidx.appcompat.widget.AppCompatEditText
{




public
EditTextSFProDisplayRegular(Context
context,
AttributeSet
attrs,
int
defStyle)
{








super(context,
attrs,
defStyle);








init();




}





public
EditTextSFProDisplayRegular(Context
context,
AttributeSet
attrs)
{








super(context,
attrs);








init();




}





public
EditTextSFProDisplayRegular(Context
context)
{








super(context);








init();




}





private
void
init()
{








if
(!isInEditMode())
{












setTypeface(ResourcesCompat.getFont(getContext(), R.font.montserrat_regular));








}




}
}
