package
        customfonts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.lapak.driverlapakkite.R;

public
class
AutoCompleteTextViewSFProDisplayRegular
extends
androidx.appcompat.widget.AppCompatAutoCompleteTextView
{




public
AutoCompleteTextViewSFProDisplayRegular(Context
context,
AttributeSet
attrs,
int
defStyle)
{








super(context,
attrs,
defStyle);








init();




}





public
AutoCompleteTextViewSFProDisplayRegular(Context
context,
AttributeSet
attrs)
{








super(context,
attrs);








init();




}





public
AutoCompleteTextViewSFProDisplayRegular(Context
context)
{








super(context);








init();




}





private
void
init()
{








if
(!isInEditMode())
{












setTypeface(ResourcesCompat.getFont(getContext(), R.font.montserrat_regular));








}




}
}
