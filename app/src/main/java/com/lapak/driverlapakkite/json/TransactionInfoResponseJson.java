package com.lapak.driverlapakkite.json;

import com.lapak.driverlapakkite.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public class TransactionInfoResponseJson {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("data")
    @Expose
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return this.data;
    }

    public void setData(final Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("reference_id")
        @Expose
        private String referenceId;

        @SerializedName("user_id")
        @Expose
        private String userId;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("type_user")
        @Expose
        private String typeUser;

        public String getTypeUser() {
            return this.typeUser;
        }

        public void setTypeUser(final String typeUser) {
            this.typeUser = typeUser;
        }

        public String getReferenceId() {
            return this.referenceId;
        }

        public void setReferenceId(final String referenceId) {
            this.referenceId = referenceId;
        }

        public String getUserId() {
            return this.userId;
        }

        public void setUserId(final String userId) {
            this.userId = userId;
        }

        public String getName() {
            return this.name;
        }

        public void setName(final String name) {
            this.name = name;
        }
    }
}
