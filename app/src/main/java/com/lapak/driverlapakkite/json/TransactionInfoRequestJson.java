package com.lapak.driverlapakkite.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ourdevelops Team on 24/02/2019.
 */

public class TransactionInfoRequestJson {

    @SerializedName("reference_id")
    @Expose
    private String referenceId;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type_user")
    @Expose
    private String typeUser;

    @SerializedName("amount")
    @Expose
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTypeUser() {
        return this.typeUser;
    }

    public void setTypeUser(final String typeUser) {
        this.typeUser = typeUser;
    }

    public String getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(final String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
