package com.lapak.driverlapakkite.json;

import java.util.List;

public class NotificationResponseJson {
    private boolean success;
    private String message;
    private List<Notification> data;

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public List<Notification> getNotification() {
        return this.data;
    }

    public void setNotification(final List notification) {
        this.data = notification;
    }

    public static class Notification {
        private long id;
        private String title;
        private String message;
        private String topic;

        public long getId() {
            return this.id;
        }

        public void setId(final int id) {
            this.id = id;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(final String title) {
            this.title = title;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(final String message) {
            this.message = message;
        }

        public String getTopic() {
            return this.topic;
        }

        public void setTopic(final String topic) {
            this.topic = topic;
        }
    }
}
