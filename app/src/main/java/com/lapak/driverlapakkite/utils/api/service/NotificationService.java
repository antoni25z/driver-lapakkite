package com.lapak.driverlapakkite.utils.api.service;

import com.lapak.driverlapakkite.json.fcm.ChatRequestJson;
import com.lapak.driverlapakkite.json.fcm.DefaultResponseJson;
import com.lapak.driverlapakkite.models.Notif;
import com.lapak.driverlapakkite.models.OrderFCM;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NotificationService {
    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body ChatRequestJson param);

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body OrderFCM param);

    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body Notif param);
}
