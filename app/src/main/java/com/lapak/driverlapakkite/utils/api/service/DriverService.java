package com.lapak.driverlapakkite.utils.api.service;

import com.lapak.driverlapakkite.json.AcceptRequestJson;
import com.lapak.driverlapakkite.json.AcceptResponseJson;
import com.lapak.driverlapakkite.json.AllTransResponseJson;
import com.lapak.driverlapakkite.json.BankResponseJson;
import com.lapak.driverlapakkite.json.ChangePassRequestJson;
import com.lapak.driverlapakkite.json.DetailRequestJson;
import com.lapak.driverlapakkite.json.DetailTransResponseJson;
import com.lapak.driverlapakkite.json.EditVehicleRequestJson;
import com.lapak.driverlapakkite.json.EditprofileRequestJson;
import com.lapak.driverlapakkite.json.GetHomeRequestJson;
import com.lapak.driverlapakkite.json.GetHomeResponseJson;
import com.lapak.driverlapakkite.json.GetOnRequestJson;
import com.lapak.driverlapakkite.json.JobResponseJson;
import com.lapak.driverlapakkite.json.LoginRequestJson;
import com.lapak.driverlapakkite.json.LoginResponseJson;
import com.lapak.driverlapakkite.json.NotificationResponseJson;
import com.lapak.driverlapakkite.json.PrivacyRequestJson;
import com.lapak.driverlapakkite.json.PrivacyResponseJson;
import com.lapak.driverlapakkite.json.RegisterRequestJson;
import com.lapak.driverlapakkite.json.RegisterResponseJson;
import com.lapak.driverlapakkite.json.ResponseJson;
import com.lapak.driverlapakkite.json.TopupRequestJson;
import com.lapak.driverlapakkite.json.TopupResponseJson;
import com.lapak.driverlapakkite.json.TransactionInfoRequestJson;
import com.lapak.driverlapakkite.json.TransactionInfoResponseJson;
import com.lapak.driverlapakkite.json.UpdateLocationRequestJson;
import com.lapak.driverlapakkite.json.VerifyRequestJson;
import com.lapak.driverlapakkite.json.WalletRequestJson;
import com.lapak.driverlapakkite.json.WalletResponseJson;
import com.lapak.driverlapakkite.json.WithdrawRequestJson;
import com.lapak.driverlapakkite.json.WithdrawResponseJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface DriverService {

    @POST("driver/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("driver/update_location")
    Call<ResponseJson> updatelocation(@Body UpdateLocationRequestJson param);

    @POST("driver/syncronizing_account")
    Call<GetHomeResponseJson> home(@Body GetHomeRequestJson param);

    @POST("driver/logout")
    Call<GetHomeResponseJson> logout(@Body GetHomeRequestJson param);

    @POST("driver/turning_on")
    Call<ResponseJson> turnon(@Body GetOnRequestJson param);

    @POST("driver/accept")
    Call<AcceptResponseJson> accept(@Body AcceptRequestJson param);

    @POST("driver/start")
    Call<AcceptResponseJson> startrequest(@Body AcceptRequestJson param);

    @POST("driver/finish")
    Call<AcceptResponseJson> finishrequest(@Body AcceptRequestJson param);

    @POST("driver/edit_profile")
    Call<LoginResponseJson> editProfile(@Body EditprofileRequestJson param);

    @POST("driver/edit_kendaraan")
    Call<LoginResponseJson> editKendaraan(@Body EditVehicleRequestJson param);

    @POST("driver/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("driver/history_progress")
    Call<AllTransResponseJson> history(@Body DetailRequestJson param);

    @POST("driver/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("driver/register_driver")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @POST("customerapi/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("driver/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

    @POST("driver/job")
    Call<JobResponseJson> job();


    @POST("customerapi/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("customerapi/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("driver/withdraw")
    Call<WithdrawResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("customerapi/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("driver/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("driver/verifycode")
    Call<ResponseJson> verifycode(@Body VerifyRequestJson param);

    @POST("customerapi/createtransaction")
    Call<TransactionInfoResponseJson> createTransaction(@Body TransactionInfoRequestJson param);
    @GET("driver/getdrivernotification")
    Call<NotificationResponseJson> getDriverNotif();


}
