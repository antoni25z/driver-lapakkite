package com.lapak.driverlapakkite.constants;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constant {

    private static final String BASE_URL = "https://lapakkite.site/";
    public static final String CONNECTION = BASE_URL + "api/";
    public static final String IMAGESFITUR = BASE_URL + "images/service/";
    public static final String IMAGESBANK = BASE_URL + "images/bank/";
    public static final String IMAGESDRIVER = BASE_URL + "images/driverphoto/";
    public static final String IMAGESUSER = BASE_URL + "images/customer/";
    public static final String IMAGESMERCHANT = BASE_URL + "images/merchant/";
    public static String CURRENCY = "";

    public static long duration;

    public static String TOKEN = "token";

    public static String USERID = "uid";

    public static String PREF_NAME = "pref_name";

    public static int permission_camera_code = 786;
    public static int permission_write_data = 788;
    public static int permission_Read_data = 789;
    public static int permission_Recording_audio = 790;

    public static SimpleDateFormat df =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static String versionname = "1.0";

    public static String UPDATED_DATE = "UPDATED_DATE";
    public static String PAYMENT_CODE = "PAYMENT_CODE";
    public static String REFERENCE_ID = "REFERENCE_ID";
    public static String TOTAL = "TOTAL";
    public static String CUSTOM_MAPBOX_STYLE = "mapbox://styles/irfan966/clh340ls400nq01qy5qrtckl4";

}
