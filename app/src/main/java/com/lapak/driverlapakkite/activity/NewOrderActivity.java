package com.lapak.driverlapakkite.activity;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.databinding.ActivityNewOrderBinding;
import com.lapak.driverlapakkite.json.AcceptRequestJson;
import com.lapak.driverlapakkite.json.AcceptResponseJson;
import com.lapak.driverlapakkite.json.fcm.DefaultResponseJson;
import com.lapak.driverlapakkite.models.OrderFCM;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.AudioPlayer;
import com.lapak.driverlapakkite.utils.PicassoTrustAll;
import com.lapak.driverlapakkite.utils.SettingPreference;
import com.lapak.driverlapakkite.utils.Utility;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;
import com.lapak.driverlapakkite.utils.api.service.NotificationService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

@SuppressLint("NonConstantResourceId")
public class NewOrderActivity extends AppCompatActivity {

    ActivityNewOrderBinding binding;
    String waktuorder,iconfitur, layanan, layanandesc, alamatasal, alamattujuan, estimasitime, hargatotal, cost, distance, idtrans, regid, orderfitur,tokenmerchant,idpelanggan,idtransmerchant;
    String wallett;
    SettingPreference sp;
    AudioPlayer audioPlayer;
    String adminFee;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        removeNotif();
        setScreenOnFlags();
        sp = new SettingPreference(this);
        sp.updateNotif("ON");
        audioPlayer = new AudioPlayer(this);
        audioPlayer.playRingtone();
        Intent intent = getIntent();
        iconfitur = intent.getStringExtra("icon");
        layanan = intent.getStringExtra("layanan");
        layanandesc = intent.getStringExtra("layanandesc");
        alamatasal = intent.getStringExtra("pickup_address");
        alamattujuan = intent.getStringExtra("destination_address");
        estimasitime = intent.getStringExtra("estimate_time");
        hargatotal = intent.getStringExtra("price");
        adminFee = intent.getStringExtra("adminFee");
        cost = intent.getStringExtra("cost");
        distance = intent.getStringExtra("distance");
        idtrans = intent.getStringExtra("transaction_id");
        regid = intent.getStringExtra("reg_id");
        wallett = intent.getStringExtra("wallet_payment");
        orderfitur = intent.getStringExtra("service_order");
        tokenmerchant = intent.getStringExtra("merchant_token");
        idpelanggan = intent.getStringExtra("customer_id");
        idtransmerchant = intent.getStringExtra("merchant_transaction_id");
        waktuorder = intent.getStringExtra("order_time");

        Utility.currencyTXT(binding.fee, adminFee, this);

        if (orderfitur.equalsIgnoreCase("3")) {
            binding.lldestination.setVisibility(View.GONE);
            binding.lldistance.setVisibility(View.GONE);

        }
        if (orderfitur.equalsIgnoreCase("4")) {

            binding.service.setText(estimasitime);
            binding.time.setText("Pelapak");
            binding.distancetext.setText("Ongkir");
            binding.costtext.setText("Biaya Pesanan");
            Utility.currencyTXT(binding.distancetext, distance, this);
            Utility.currencyTXT(binding.cost, cost, this);
        } else {

            binding.service.setText(estimasitime);
            binding.distancetext.setText(distance);
            binding.cost.setText(cost);
        }
        binding.layanan.setText(layanan);
        binding.layanandes.setText(layanandesc);
        binding.pickUpText.setText(alamatasal);
        binding.destinationText.setText(alamattujuan);
        Utility.currencyTXT(binding.price, hargatotal, this);
        if (wallett.equalsIgnoreCase("true")) {
            binding.totaltext.setText("Total (WALLET)");
        } else {
            binding.totaltext.setText("Total (CASH)");
        }


        PicassoTrustAll.getInstance(this)
                .load(Constant.IMAGESFITUR + iconfitur)
                .placeholder(R.drawable.lapak)
                .into(binding.image);

        timerplay.start();

        binding.cancel.setOnClickListener(view -> {
            audioPlayer.stopRingtone();
            sp.updateNotif("OFF");
            timerplay.cancel();
            Intent toOrder = new Intent(NewOrderActivity.this, MainActivity.class);
            toOrder.addFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(toOrder);

        });

        if (new SettingPreference(this).getSetting()[0].equals("OFF")) {
            binding.order.setOnClickListener(view -> getaccept());
        } else {
            getaccept();
        }


    }

    CountDownTimer timerplay = new CountDownTimer(Constant.duration, 1000) {

        @SuppressLint("SetTextI18n")
        public void onTick(long millisUntilFinished) {
            binding.timer.setText("" + millisUntilFinished / 1000);
        }


        public void onFinish() {
            audioPlayer.stopRingtone();
            sp.updateNotif("OFF");
            binding.timer.setText("0");
            Intent toOrder = new Intent(NewOrderActivity.this, MainActivity.class);
            toOrder.addFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(toOrder);
        }
    }.start();




    @Override
    public void onBackPressed() {
    }

    private void getaccept() {
        audioPlayer.stopRingtone();
        timerplay.cancel();
        binding.rlprogress.setVisibility(View.VISIBLE);
        final User loginUser = BaseApp.getInstance(this).getLoginUser();
        DriverService userService = ServiceGenerator.createService(
                DriverService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        AcceptRequestJson param = new AcceptRequestJson();
        param.setId(loginUser.getId());
        param.setIdtrans(idtrans);
        userService.accept(param).enqueue(new Callback<AcceptResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<AcceptResponseJson> call, @NonNull final Response<AcceptResponseJson> response) {
                if (response.isSuccessful()) {
                    sp.updateNotif("OFF");
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("berhasil")) {
                        binding.rlprogress.setVisibility(View.GONE);
                        Intent i = new Intent(NewOrderActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        OrderFCM.Data orderfcm = new OrderFCM.Data();
                        orderfcm.driver_id = loginUser.getId();
                        orderfcm.transaction_id = idtrans;
                        orderfcm.response = "2";
                        if (orderfitur.equalsIgnoreCase("4")) {
                            orderfcm.desc = "pengemudi sedang membeli pesanan";
                            orderfcm.customer_id = idpelanggan;
                            orderfcm.invoice = "INV-"+idtrans+idtransmerchant;
                            orderfcm.ordertime = waktuorder;
                            sendMessageToDriver(tokenmerchant, orderfcm);
                        } else {
                            orderfcm.desc = getString(R.string.notification_start);
                        }
                        sendMessageToDriver(regid, orderfcm);
                    } else {
                        sp.updateNotif("OFF");
                        binding.rlprogress.setVisibility(View.GONE);
                        Intent i = new Intent(NewOrderActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        Toast.makeText(NewOrderActivity.this, "Pesanan tidak lagi tersedia!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AcceptResponseJson> call, @NonNull Throwable t) {
                Toast.makeText(NewOrderActivity.this, "Error Connection!", Toast.LENGTH_SHORT).show();
                binding.rlprogress.setVisibility(View.GONE);
                sp.updateNotif("OFF");
                binding.rlprogress.setVisibility(View.GONE);
                Intent i = new Intent(NewOrderActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
    }

    private void sendMessageToDriver(final String regIDTujuan, final OrderFCM.Data response) {

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        OrderFCM orderFCM = new OrderFCM(regIDTujuan, response);

        service.sendNotification(orderFCM).enqueue(new Callback<DefaultResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponseJson> call, @NonNull Response<DefaultResponseJson> response) {
                Timber.tag("response_d34").d(Objects.requireNonNull(response.body()).getMessage());
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponseJson> call, @NonNull Throwable t) {
                Timber.tag("response_d34").d(Objects.requireNonNull(t.getMessage()));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.updateNotif("OFF");
    }

    private void removeNotif() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Objects.requireNonNull(notificationManager).cancel(100);
    }

    private void setScreenOnFlags() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
            setTurnScreenOn(true);
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            Objects.requireNonNull(keyguardManager).requestDismissKeyguard(this, null);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        }
    }
}
