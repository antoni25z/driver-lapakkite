package com.lapak.driverlapakkite.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.lapak.driverlapakkite.json.TransactionInfoRequestJson;
import com.lapak.driverlapakkite.json.TransactionInfoResponseJson;
import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.Log;
import com.lapak.driverlapakkite.utils.SettingPreference;
import com.lapak.driverlapakkite.utils.Utility;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;
import com.wensolution.wensxendit.PaymentMethod;
import com.wensolution.wensxendit.WensXendit;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopupSaldoActivity extends AppCompatActivity {

    EditText nominal;
    ImageView text1, text2, text3, text4;
    RelativeLayout rlnotif, rlprogress;
    TextView textnotif;
    String disableback;
    SettingPreference sp;
    Button choosePm;

    WensXendit wensXendit;
    ActivityResultLauncher<Intent> paymentLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);

        sp = new SettingPreference(this);

        nominal = findViewById(R.id.balance);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        rlnotif = findViewById(R.id.rlnotif);
        textnotif = findViewById(R.id.textnotif);
        rlprogress = findViewById(R.id.rlprogress);
        choosePm = findViewById(R.id.choose_pm_btn);

        Log.d("2504", sp.getSetting()[23]);

        wensXendit = new WensXendit(this);
        wensXendit.setXenditApiKey(sp.getSetting()[22]);
        wensXendit.setIlumaApiKey(sp.getSetting()[23]);
        wensXendit.setActiveMethods(new String[]{
                PaymentMethod.BRI,
                PaymentMethod.MANDIRI,
                PaymentMethod.BSI,
                PaymentMethod.PERMATA,
                PaymentMethod.BNI,
                PaymentMethod.BJB,
                PaymentMethod.DANA,
                PaymentMethod.OVO,
                PaymentMethod.SHOPEEPAY,
                PaymentMethod.LINKAJA,
                PaymentMethod.ASTRAPAY,
                PaymentMethod.QRIS,
        });

        nominal.addTextChangedListener(Utility.currencyTW(nominal,this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());


        choosePm.setOnClickListener(view -> createTransaction());

        text1.setOnClickListener(v -> nominal.setText("20000"));

        text2.setOnClickListener(v -> nominal.setText("50000"));

        text3.setOnClickListener(v -> nominal.setText("100000"));

        text4.setOnClickListener(v -> nominal.setText("200000"));

        disableback = "false";
    }


    public void notif(String text) {
        rlnotif.setVisibility(View.VISIBLE);
        textnotif.setText(text);

        new Handler().postDelayed(() -> rlnotif.setVisibility(View.GONE), 3000);
    }


    @Override
    public void onBackPressed() {
        if (!disableback.equals("true")) {
            finish();
        }
    }

    private void createTransaction() {
        String referenceId = "pm-level-" + UUID.randomUUID();
        if (!nominal.getText().toString().isEmpty()) {
            progressshow();
            String paymentAmount = nominal.getText().toString();
            final User user = BaseApp.getInstance(this).getLoginUser();
            TransactionInfoRequestJson request = new TransactionInfoRequestJson();
            request.setReferenceId(referenceId);
            request.setUserId(user.getId());
            request.setName(user.getFullnama());
            request.setTypeUser("driver");
            request.setAmount(convertAngka(paymentAmount));

            DriverService service = ServiceGenerator.createService(DriverService.class, user.getNoTelepon(), user.getPassword());
            service.createTransaction(request).enqueue(new Callback<TransactionInfoResponseJson>() {
                @Override
                public void onResponse(@NonNull Call<TransactionInfoResponseJson> call, @NonNull Response<TransactionInfoResponseJson> response) {
                    progresshide();
                    if (response.isSuccessful()) {
                        wensXendit.startPayment(
                                Long.parseLong(convertAngka(paymentAmount)),
                                response.body().getData().getReferenceId(),
                                response.body().getData().getName()
                        );
                    } else {
                        notif("error");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TransactionInfoResponseJson> call, @NonNull Throwable t) {
                    progresshide();
                    notif(t.getMessage());
                }
            });
        } else {
            notif("Nominal Kosong !");
        }
    }

    public void progressshow() {
        rlprogress.setVisibility(View.VISIBLE);
        disableback = "true";
    }

    public void progresshide() {
        rlprogress.setVisibility(View.GONE);
        disableback = "false";
    }

    public String convertAngka(String value) {
        return (((((value + "")
                .replaceAll(sp.getSetting()[0], ""))
                .replaceAll(" ", ""))
                .replaceAll(",", ""))
                .replaceAll("[Rp.]", ""));
    }
}
