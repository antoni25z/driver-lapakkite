package com.lapak.driverlapakkite.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.databinding.ActivityConfirmWithdrawBinding;
import com.lapak.driverlapakkite.json.ResponseJson;
import com.lapak.driverlapakkite.json.TransactionInfoRequestJson;
import com.lapak.driverlapakkite.json.TransactionInfoResponseJson;
import com.lapak.driverlapakkite.json.WithdrawRequestJson;
import com.lapak.driverlapakkite.json.WithdrawResponseJson;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.SettingPreference;
import com.lapak.driverlapakkite.utils.Utility;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;
import com.wensolution.wensxendit.WensXendit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmWithdrawActivity extends AppCompatActivity {

    private ActivityConfirmWithdrawBinding binding;

    boolean disableback = false;

    SettingPreference sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConfirmWithdrawBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disableback = false;

        String holderName = getIntent().getStringExtra("holder_name");
        String bankName = getIntent().getStringExtra("bank_name");
        String bankNumber = getIntent().getStringExtra("bank_number");
        String disburseCode = getIntent().getStringExtra("disburse_code");

        sp = new SettingPreference(this);

        WensXendit wensXendit = new WensXendit(this);
        wensXendit.setXenditApiKey(sp.getSetting()[22]);
        wensXendit.setIlumaApiKey(sp.getSetting()[23]);

        binding.holderNameTxt.setText(holderName);
        binding.bankNameTxt.setText(bankName);
        binding.bankNumberTxt.setText(bankNumber);

        binding.amountEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isBlank()) {
                    binding.totalTxt.setText("0");
                } else {
                    int total = Integer.parseInt(charSequence.toString()) + 5000;
                    Utility.currencyTXT(binding.totalTxt, String.valueOf(total), ConfirmWithdrawActivity.this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.withdrawBtn.setOnClickListener(view -> {
            String amount = Objects.requireNonNull(binding.amountEdt.getText()).toString();
            String total = convertAngka(binding.totalTxt.getText().toString());
            String externalId = "disb-" + UUID.randomUUID();

            if (amount.isBlank()) {
                Toast.makeText(this, "Fill the field", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Apakah anda yakin ingin melakukan penarikan dengan jumlah " + amount + " ke rekening atas nama " + holderName);
                builder.setTitle("Penarikan");
                builder.setPositiveButton("Kirim", (dialogInterface, i) -> {
                    progressshow();
                    final User user = BaseApp.getInstance(this).getLoginUser();
                    TransactionInfoRequestJson request = new TransactionInfoRequestJson();
                    request.setReferenceId(externalId);
                    request.setUserId(user.getId());
                    request.setName(user.getFullnama());
                    request.setTypeUser("driver");
                    request.setAmount(total);

                    DriverService service = ServiceGenerator.createService(DriverService.class, user.getNoTelepon(), user.getPassword());
                    service.createTransaction(request).enqueue(new Callback<TransactionInfoResponseJson>() {
                        @Override
                        public void onResponse(Call<TransactionInfoResponseJson> call, Response<TransactionInfoResponseJson> response) {
                            if (response.isSuccessful()) {
                                WithdrawRequestJson request = new WithdrawRequestJson();
                                request.setId(user.getId());
                                request.setBank(bankName);
                                request.setName(holderName);
                                request.setAmount(total);
                                request.setCard(bankNumber);
                                request.setType("withdraw");
                                request.setIdWallet(externalId);

                                service.withdraw(request).enqueue(new Callback<>() {
                                    @Override
                                    public void onResponse(@NonNull Call<WithdrawResponseJson> call, @NonNull Response<WithdrawResponseJson> response) {
                                        if (response.isSuccessful()) {
                                            if (Objects.requireNonNull(response.body()).isSuccess()) {
                                                wensXendit.createDisbursement(externalId, Long.parseLong(amount), disburseCode, holderName, bankNumber, (message, success) -> {
                                                    progresshide();
                                                    if (success) {
                                                        Toast.makeText(ConfirmWithdrawActivity.this, "Penarikan Sedang Diproses", Toast.LENGTH_SHORT).show();
                                                        finish();
                                                    } else {
                                                        notif(message);
                                                    }
                                                    return null;
                                                });
                                            } else {
                                                progresshide();
                                                Toast.makeText(ConfirmWithdrawActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            progresshide();
                                            notif("Kesalahan!");
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<WithdrawResponseJson> call, @NonNull Throwable t) {
                                        progresshide();
                                        t.printStackTrace();
                                        notif("error");
                                    }
                                });
                            } else {
                                progresshide();
                                notif("error");
                            }
                        }

                        @Override
                        public void onFailure(Call<TransactionInfoResponseJson> call, Throwable t) {
                            progresshide();
                            notif(t.getMessage());
                            t.printStackTrace();
                        }
                    });

                });
                builder.setNegativeButton("Batal", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });

                builder.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!disableback) {
            finish();
        }

    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }


    public void progressshow() {
        binding.rlprogress.setVisibility(View.VISIBLE);
        disableback = true;
    }

    public void progresshide() {
        binding.rlprogress.setVisibility(View.GONE);
        disableback = true;
    }

    public String convertAngka(String value) {
        return (((((value + "")
                .replaceAll(sp.getSetting()[0], ""))
                .replaceAll(" ", ""))
                .replaceAll(",", ""))
                .replaceAll("[Rp.]", ""));
    }
}