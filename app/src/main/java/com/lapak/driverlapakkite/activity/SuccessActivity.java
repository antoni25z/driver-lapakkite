package com.lapak.driverlapakkite.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.databinding.ActivitySuccessBinding;
import com.lapak.driverlapakkite.utils.Utility;

public class SuccessActivity extends AppCompatActivity {

    ActivitySuccessBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySuccessBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String updateDate = getIntent().getStringExtra(Constant.UPDATED_DATE);
        String paymentCode = getIntent().getStringExtra(Constant.PAYMENT_CODE);
        String referenceId = getIntent().getStringExtra(Constant.REFERENCE_ID);
        String total = getIntent().getStringExtra(Constant.TOTAL);

        Utility.currencyTXT(binding.amountTxt, total, this);
        Utility.currencyTXT(binding.amount2Txt, total, this);
        binding.paymentMethodTxt.setText(paymentCode);
        binding.referenceIdTxt.setText(referenceId);
        binding.timeTxt.setText(Utility.convertServerDateToUserTimeZone(updateDate));

        binding.doneBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}