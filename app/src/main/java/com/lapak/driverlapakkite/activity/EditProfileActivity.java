package com.lapak.driverlapakkite.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.DialogFragment;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.json.EditprofileRequestJson;
import com.lapak.driverlapakkite.json.LoginResponseJson;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.PicassoTrustAll;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yesterselga.countrypicker.CountryPicker;
import com.yesterselga.countrypicker.Theme;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    ImageView photo, gantifoto, backbtn, backButtonverify;
    EditText phone, nama, email;
    TextView tanggal, countryCode, textnotif;
    Button submit;
    RelativeLayout rlnotif;
    private SimpleDateFormat dateFormatter, dateFormatterview;
    byte[] imageByteArray;
    Bitmap decoded;
    String dateview, disableback, onsubmit;

    String country_iso_code = "en";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        photo = findViewById(R.id.photo);
        gantifoto = findViewById(R.id.editfoto);
        backbtn = findViewById(R.id.back_btn);
        phone = findViewById(R.id.phonenumber);
        nama = findViewById(R.id.nama);
        email = findViewById(R.id.email);
        tanggal = findViewById(R.id.tanggal);
        submit = findViewById(R.id.submit);
        rlnotif = findViewById(R.id.rlnotif);
        textnotif = findViewById(R.id.textnotif);
        countryCode = findViewById(R.id.countrycode);
        backButtonverify = findViewById(R.id.back_btn_verify);
        backbtn.setOnClickListener(v -> finish());
        onsubmit = "true";

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        PicassoTrustAll.getInstance(this)
                .load(Constant.IMAGESDRIVER + loginUser.getFotodriver())
                .placeholder(R.drawable.image_placeholder)
                .into(photo);

        phone.setText(loginUser.getPhone());
        nama.setText(loginUser.getFullnama());
        email.setText(loginUser.getEmail());
        dateview = loginUser.getTglLahir();
        countryCode.setText(loginUser.getCountrycode());

        gantifoto.setOnClickListener(v -> selectImage());

        countryCode.setOnClickListener(v -> {
            final CountryPicker picker = CountryPicker.newInstance("Select Country", Theme.LIGHT);
            picker.setListener((name, code, dialCode, flagDrawableResID) -> {
                countryCode.setText(dialCode);
                picker.dismiss();
                country_iso_code = code;
            });
            picker.setStyle(DialogFragment.STYLE_NORMAL, R.style.countrypicker_style);
            picker.show(getSupportFragmentManager(), "Select Country");
        });

        Date myDate = null;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormatterview = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try {
            if (loginUser.getTglLahir() != null) {
                myDate = dateFormatter.parse(loginUser.getTglLahir());
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        String finalDate = dateFormatterview.format(Objects.requireNonNull(myDate));
        tanggal.setText(finalDate);
        submit.setOnClickListener(v -> {
            final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            final String emailvalidate = email.getText().toString();

            if (TextUtils.isEmpty(phone.getText().toString())) {

                notif(getString(R.string.phoneempty));

            } else if (TextUtils.isEmpty(nama.getText().toString())) {

                notif("Nama tidak boleh kosong");

            } else if (TextUtils.isEmpty(email.getText().toString())) {

                notif(getString(R.string.emailempty));

            } else if (TextUtils.isEmpty(tanggal.getText().toString())) {

                notif("Tanggal lahir tidak boleh kosong!");

            } else if (!emailvalidate.matches(emailPattern)) {

                notif("Format email salah!");

            } else {
                if (onsubmit.equals("true")) {
                    submit.setText(getString(R.string.waiting_pleaseWait));
                    submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.rounded_corners_button, getTheme()));
                    editprofile();
                }
            }
        });

        tanggal.setOnClickListener(v -> showTanggal());
        disableback = "false";

    }

    private void showTanggal() {

        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    long date_ship_millis = calendar.getTimeInMillis();
                    tanggal.setText(dateFormatterview.format(date_ship_millis));
                    dateview = dateFormatter.format(date_ship_millis);
                }
        );
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorgradient));
        datePicker.show(getSupportFragmentManager(), "Datepickerdialog");
    }


    public void notif(String text) {
        rlnotif.setVisibility(View.VISIBLE);
        textnotif.setText(text);

        new Handler().postDelayed(() -> rlnotif.setVisibility(View.GONE), 3000);
    }


    /**
     * uploadfoto-------------start.
     */
    private boolean check_ReadStoragepermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constant.permission_Read_data);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        return false;
    }

    private void selectImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (check_ReadStoragepermission()) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public String getPath(Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 2) {
                Uri selectedImage = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(Objects.requireNonNull(selectedImage));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                final Bitmap imagebitmap = BitmapFactory.decodeStream(imageStream);
                Bitmap scaleBitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * 0.1), (int) (imagebitmap.getHeight()*0.1) ,  true);

                String path = getPath(selectedImage);
                Matrix matrix = new Matrix();
                ExifInterface exif;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    try {
                        exif = new ExifInterface(path);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                matrix.postRotate(90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                matrix.postRotate(180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                matrix.postRotate(270);
                                break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    photo.setImageBitmap(bitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    imageByteArray = baos.toByteArray();
                    decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()));

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

            }

        }

    }

    private void editprofile() {
        onsubmit = "false";
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        EditprofileRequestJson request = new EditprofileRequestJson();
        request.setFullNama(nama.getText().toString());
        request.setEmail(email.getText().toString());
        request.setEmaillama(loginUser.getEmail());
        request.setId(loginUser.getId());
        request.setNoTelepon(countryCode.getText().toString().replace("+", "") + phone.getText().toString());
        request.setPhone(phone.getText().toString());
        request.setCountrycode(countryCode.getText().toString());
        request.setPhonelama(loginUser.getNoTelepon());
        if (imageByteArray != null) {
            request.setFotopelangganlama(loginUser.getFotodriver());
            request.setFotopelanggan(getStringImage(decoded));
        }
        request.setTglLahir(dateview);


        DriverService service = ServiceGenerator.createService(DriverService.class, loginUser.getEmail(), loginUser.getPassword());
        service.editProfile(request).enqueue(new Callback<LoginResponseJson>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<LoginResponseJson> call, @NonNull Response<LoginResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        User user = response.body().getData().get(0);
                        saveUser(user);
                        finish();
                        onsubmit = "true";

                    } else {
                        onsubmit = "true";
                        submit.setText("Submit");
                        submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_1, getTheme()));
                        notif(response.body().getMessage());
                    }
                } else {
                    onsubmit = "true";
                    submit.setText("Submit");
                    submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_1, getTheme()));
                    notif("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                notif("error!");
            }
        });
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        imageByteArray = baos.toByteArray();
        return Base64.encodeToString(imageByteArray, Base64.DEFAULT);
    }

    private void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(User.class);
        realm.copyToRealm(user);
        realm.commitTransaction();
        BaseApp.getInstance(EditProfileActivity.this).setLoginUser(user);
    }
}
