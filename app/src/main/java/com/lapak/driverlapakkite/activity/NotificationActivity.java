package com.lapak.driverlapakkite.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.item.NotifAdapter;
import com.lapak.driverlapakkite.json.NotificationResponseJson;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.Log;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    ShimmerFrameLayout shimmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        RecyclerView notifRv = findViewById(R.id.notif_rv);
        RelativeLayout noData = findViewById(R.id.rlnodata);
        MaterialToolbar toolbar = findViewById(R.id.materialToolbar);
        shimmer = findViewById(R.id.shimmerwallet);

        shimmershow();

        notifRv.setLayoutManager(new LinearLayoutManager(this));

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        DriverService userService = ServiceGenerator.createService(DriverService.class, loginUser.getNoTelepon(), loginUser.getPassword());

        userService.getDriverNotif().enqueue(new Callback<NotificationResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<NotificationResponseJson> call, Response<NotificationResponseJson> response) {
                shimmertutup();
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        if (response.body().getNotification().isEmpty()) {
                            noData.setVisibility(View.VISIBLE);
                            notifRv.setVisibility(View.GONE);
                        } else {
                            notifRv.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            notifRv.setAdapter(new NotifAdapter(response.body().getNotification()));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationResponseJson> call, Throwable t) {
                Log.d("2504", t.getMessage());
            }
        });

        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });
    }

    private void shimmershow() {
        shimmer.setVisibility(View.VISIBLE);
        shimmer.startShimmer();
    }

    private void shimmertutup() {

        shimmer.setVisibility(View.GONE);
        shimmer.stopShimmer();
    }
}