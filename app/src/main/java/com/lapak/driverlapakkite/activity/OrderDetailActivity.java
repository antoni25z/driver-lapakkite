package com.lapak.driverlapakkite.activity;

import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.expressions.Expression.color;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lineProgress;
import static com.mapbox.mapboxsdk.style.expressions.Expression.linear;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.match;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineGradient;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.lapak.driverlapakkite.databinding.ActivityDetailOrderBinding;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;
import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.item.ItemPesananItem;
import com.lapak.driverlapakkite.json.DetailRequestJson;
import com.lapak.driverlapakkite.json.DetailTransResponseJson;
import com.lapak.driverlapakkite.models.CustomerModel;
import com.lapak.driverlapakkite.models.TransModel;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.Log;
import com.lapak.driverlapakkite.utils.PicassoTrustAll;
import com.lapak.driverlapakkite.utils.Utility;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;

import java.util.Locale;
import java.util.Objects;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

@SuppressLint("NonConstantResourceId")
public class OrderDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final int REQUEST_PERMISSION_CALL = 992;
    String idtrans, idpelanggan, response, service;
    ItemPesananItem itemPesananItem;
    TextView totaltext;
    MapView mapView;

    Point destination, pickup;
    ActivityDetailOrderBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomSheetBehavior behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(300);
        binding.status.setVisibility(View.VISIBLE);
        binding.backBtn.setVisibility(View.VISIBLE);
        binding.llbutton.setVisibility(View.GONE);
        binding.llchat.setVisibility(View.GONE);
        binding.llchatmerchant.setVisibility(View.GONE);
        totaltext = findViewById(R.id.totaltext);
        binding.currentlocation.setVisibility(View.GONE);
        shimmerload();


        binding.backBtn.setOnClickListener(view -> finish());

        binding.merchantnear.setHasFixedSize(true);
        binding.merchantnear.setNestedScrollingEnabled(false);
        binding.merchantnear.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        Intent intent = getIntent();
        idpelanggan = intent.getStringExtra("customer_id");
        idtrans = intent.getStringExtra("transaction_id");
        response = intent.getStringExtra("response");

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        if (Objects.equals(response, "2")) {
            binding.llchat.setVisibility(View.VISIBLE);
            binding.layanandes.setText(getString(R.string.notification_accept));
        } else if (Objects.equals(response, "3")) {
            binding.llchat.setVisibility(View.VISIBLE);
            binding.order.setVisibility(View.GONE);
            binding.layanandes.setText(getString(R.string.notification_start));
        } else if (Objects.equals(response, "4")) {
            binding.scroller.setPadding(0, 0, 0, 10);
            binding.llchat.setVisibility(View.GONE);
            binding.order.setVisibility(View.GONE);
            binding.layanandes.setText(getString(R.string.notification_finish));
        } else if (Objects.equals(response, "5")) {
            binding.scroller.setPadding(0, 0, 0, 10);
            binding.llchat.setVisibility(View.GONE);
            binding.order.setVisibility(View.GONE);
            binding.layanandes.setText(getString(R.string.notification_cancel));
        }
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        mapboxMap.setStyle(new Style.Builder().fromUri(Constant.CUSTOM_MAPBOX_STYLE)
                .withImage("ORIGIN_ICON_ID", Objects.requireNonNull(BitmapUtils.getBitmapFromDrawable(
                        ResourcesCompat.getDrawable(getResources(), R.drawable.ic_pickup_map, null))))
                .withImage("DESTINATION_ICON_ID", Objects.requireNonNull(BitmapUtils.getBitmapFromDrawable(
                        ResourcesCompat.getDrawable(getResources(), R.drawable.ic_destination_map, null)))), style -> {

            UiSettings uiSettings = mapboxMap.getUiSettings();
            uiSettings.setCompassEnabled(false);
            uiSettings.setAttributionEnabled(false);
            uiSettings.setLogoEnabled(false);

            getData(idtrans, idpelanggan, style, mapboxMap);

        });
    }

    private void getData(final String idtrans, final String idpelanggan, Style style, MapboxMap mapboxMap) {
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        DriverService services = ServiceGenerator.createService(DriverService.class, loginUser.getEmail(), loginUser.getPassword());
        DetailRequestJson param = new DetailRequestJson();
        param.setId(idtrans);
        param.setIdPelanggan(idpelanggan);
        services.detailtrans(param).enqueue(new Callback<DetailTransResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<DetailTransResponseJson> call, @NonNull Response<DetailTransResponseJson> responsedata) {
                if (responsedata.isSuccessful()) {
                    shimmertutup();
                    final TransModel transaksi = responsedata.body().getData().get(0);
                    CustomerModel pelanggan = responsedata.body().getPelanggan().get(0);
                    String formatdistance = String.format(Locale.US, "%.1f", transaksi.getJarak());
                    binding.distance.setText(formatdistance);
                    binding.service.setText(transaksi.getEstimasi());
                    CameraPosition position = new CameraPosition.Builder()
                            .target(new LatLng(transaksi.getEndLatitude(), transaksi.getEndLongitude()))
                            .zoom(10)
                            .tilt(20)
                            .build();
                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 100);
                    service = transaksi.getOrderFitur();
                    Log.e("home", transaksi.getHome());

                    switch (transaksi.getHome()) {
                        case "3":
                            binding.lldestination.setVisibility(View.GONE);
                            binding.lldistance.setVisibility(View.GONE);
                            binding.service.setText(transaksi.getEstimasi());
                            break;
                        case "4":
                            binding.orderdetail.setVisibility(View.VISIBLE);
                            binding.merchantdetail.setVisibility(View.VISIBLE);
                            binding.llmerchantinfo.setVisibility(View.VISIBLE);
                            Utility.currencyTXT(binding.deliveryfee, String.valueOf(transaksi.getHarga() - Long.parseLong(transaksi.getTotal_biaya())), OrderDetailActivity.this);
                            Utility.currencyTXT(binding.cost, String.valueOf(transaksi.getTotal_biaya()), OrderDetailActivity.this);
                            binding.namamerchant.setText(transaksi.getNama_merchant());

                            itemPesananItem = new ItemPesananItem(responsedata.body().getItem(), R.layout.item_pesanan);
                            binding.merchantnear.setAdapter(itemPesananItem);
                            break;
                        case "2":
                            binding.senddetail.setVisibility(View.VISIBLE);
                            binding.produk.setText(transaksi.getNamaBarang());
                            binding.sendername.setText(transaksi.namaPengirim);
                            binding.receivername.setText(transaksi.namaPenerima);

                            binding.senderphone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderDetailActivity.this, R.style.DialogStyle);
                                    alertDialogBuilder.setTitle("Panggil Pengemudi");
                                    alertDialogBuilder.setMessage("Anda ingin menelepon " + transaksi.getNamaPengirim() + "(" + transaksi.teleponPengirim + ")?");
                                    alertDialogBuilder.setPositiveButton("Ya",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    if (ActivityCompat.checkSelfPermission(OrderDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                        ActivityCompat.requestPermissions(OrderDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                                        return;
                                                    }

                                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                                    callIntent.setData(Uri.parse("tel:" + transaksi.teleponPengirim));
                                                    startActivity(callIntent);
                                                }
                                            });

                                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();


                                }
                            });

                            binding.receiverphone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderDetailActivity.this, R.style.DialogStyle);
                                    alertDialogBuilder.setTitle("Panggil Pengemudi");
                                    alertDialogBuilder.setMessage("Anda ingin menelepon " + transaksi.getNamaPenerima() + "(" + transaksi.teleponPenerima + ")?");
                                    alertDialogBuilder.setPositiveButton("yes",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    if (ActivityCompat.checkSelfPermission(OrderDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                        ActivityCompat.requestPermissions(OrderDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                                        return;
                                                    }

                                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                                    callIntent.setData(Uri.parse("tel:" + transaksi.teleponPenerima));
                                                    startActivity(callIntent);
                                                }
                                            });

                                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();


                                }
                            });

                            break;
                    }


                    destination = Point.fromLngLat(transaksi.getEndLongitude(), transaksi.getEndLatitude());
                    pickup = Point.fromLngLat(transaksi.getStartLongitude(), transaksi.getStartLatitude());
                    initSources(style, "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID");
                    initLayers(style, "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID", "DROUTE_LAYER_ID", "DICON_LAYER_ID");
                    getRoute(mapboxMap, pickup, destination, "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID");


                    parsedata(transaksi, pelanggan);

                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<DetailTransResponseJson> call, @NonNull Throwable t) {

            }
        });


    }

    private void initSources(@NonNull Style loadedMapStyle, String lineid, String iconsourceid) {
        loadedMapStyle.addSource(new GeoJsonSource(lineid, new GeoJsonOptions().withLineMetrics(true)));
        loadedMapStyle.addSource(new GeoJsonSource(iconsourceid, getOriginAndDestinationFeatureCollection()));

    }

    private void initLayers(@NonNull Style loadedMapStyle, String lineid, String iconsourceid, String routeid, String iconlayerid) {
        loadedMapStyle.addLayer(new LineLayer(routeid, lineid).withProperties(
                lineCap(Property.LINE_CAP_ROUND),
                lineJoin(Property.LINE_JOIN_ROUND),
                lineWidth(6f),
                lineGradient(interpolate(
                        linear(), lineProgress(),
                        stop(0f, color(getResources().getColor(R.color.colorPrimary))),
                        stop(1f, color(getResources().getColor(R.color.colorgradient))
                        )))));

        loadedMapStyle.addLayer(new SymbolLayer(iconlayerid, iconsourceid).withProperties(
                iconImage(match(get("originDestination"), literal("origin"),
                        stop("origin", "ORIGIN_ICON_ID"),
                        stop("destination", "DESTINATION_ICON_ID"))),
                iconIgnorePlacement(true),
                iconAllowOverlap(true),
                iconSize(2.0f),
                iconOffset(new Float[]{0f, -4f})));
    }

    MapboxDirections client;
    private DirectionsRoute currentRoute;

    private void getRoute(MapboxMap mapboxMap, Point origin, Point destination, String lineid, String iconsourceid) {
        client = MapboxDirections.builder()
                .origin(origin)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_WALKING)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();
        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    return;
                } else if (response.body().routes().size() < 1) {
                    return;
                }

                currentRoute = response.body().routes().get(0);

                if (currentRoute != null) {
                    if (mapboxMap != null) {
                        mapboxMap.getStyle(style -> {
                            GeoJsonSource originDestinationPointGeoJsonSource = style.getSourceAs(iconsourceid);

                            if (originDestinationPointGeoJsonSource != null) {
                                originDestinationPointGeoJsonSource.setGeoJson(getOriginAndDestinationFeatureCollection());
                            }
                            GeoJsonSource lineLayerRouteGeoJsonSource = style.getSourceAs(lineid);

                            if (lineLayerRouteGeoJsonSource != null) {
                                LineString lineString = LineString.fromPolyline(Objects.requireNonNull(currentRoute.geometry()), PRECISION_6);
                                lineLayerRouteGeoJsonSource.setGeoJson(Feature.fromGeometry(lineString));
                            }
                        });
                    }
                } else {
                    Timber.d("Directions route is null");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
            }
        });
    }

    private FeatureCollection getOriginAndDestinationFeatureCollection() {
        Feature origin = Feature.fromGeometry(pickup);
        origin.addStringProperty("originDestination", "origin");
        Feature destinationFeature = Feature.fromGeometry(destination);
        destinationFeature.addStringProperty("originDestination", "destination");
        return FeatureCollection.fromFeatures(new Feature[]{origin, destinationFeature});
    }

    private void shimmerload() {
        binding.shimmerlayanan.startShimmer();
        binding.shimmerpickup.startShimmer();
        binding.shimmerdestination.startShimmer();
        binding.shimmerfitur.startShimmer();
        binding.shimmerdistance.startShimmer();
        binding.shimmerprice.startShimmer();
        binding.shimmerincash.startShimmer();
        binding.shimmerinwallet.startShimmer();

        binding.layanan.setVisibility(View.GONE);
        binding.layanandes.setVisibility(View.GONE);
        binding.pickUpText.setVisibility(View.GONE);
        binding.destinationText.setVisibility(View.GONE);
        binding.service.setVisibility(View.GONE);
        binding.price.setVisibility(View.GONE);
        binding.incash.setVisibility(View.GONE);
        binding.inwallet.setVisibility(View.GONE);
    }

    private void shimmertutup() {
        binding.shimmerlayanan.stopShimmer();
        binding.shimmerpickup.stopShimmer();
        binding.shimmerdestination.stopShimmer();
        binding.shimmerfitur.stopShimmer();
        binding.shimmerdistance.stopShimmer();
        binding.shimmerprice.stopShimmer();
        binding.shimmerincash.stopShimmer();
        binding.shimmerinwallet.stopShimmer();

        binding.shimmerlayanan.setVisibility(View.GONE);
        binding.shimmerpickup.setVisibility(View.GONE);
        binding.shimmerdestination.setVisibility(View.GONE);
        binding.shimmerfitur.setVisibility(View.GONE);
        binding.shimmerdistance.setVisibility(View.GONE);
        binding.shimmerprice.setVisibility(View.GONE);
        binding.shimmerincash.setVisibility(View.GONE);
        binding.shimmerinwallet.setVisibility(View.GONE);

        binding.layanan.setVisibility(View.VISIBLE);
        binding.layanandes.setVisibility(View.VISIBLE);
        binding.pickUpText.setVisibility(View.VISIBLE);
        binding.destinationText.setVisibility(View.VISIBLE);
        binding.distance.setVisibility(View.VISIBLE);
        binding.service.setVisibility(View.VISIBLE);
        binding.price.setVisibility(View.VISIBLE);
        binding.incash.setVisibility(View.VISIBLE);
        binding.inwallet.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    private void parsedata(TransModel request, final CustomerModel pelanggan) {
        binding.rlprogress.setVisibility(View.GONE);

        PicassoTrustAll.getInstance(this)
                .load(Constant.IMAGESUSER + pelanggan.getFoto())
                .placeholder(R.drawable.image_placeholder)
                .into(binding.background);

        long totalBiaya = (Long.parseLong(request.final_cost) + request.getAdminFee());

        if (request.isPakaiWallet()) {
            Utility.currencyTXT(binding.incash, "0", this);
            Utility.currencyTXT(binding.inwallet, String.valueOf(totalBiaya), this);
        } else {
            Utility.currencyTXT(binding.inwallet, "0", this);
            Utility.currencyTXT(binding.incash, String.valueOf(totalBiaya), this);
        }

        if (!request.getRate().isEmpty()) {
            binding.ratingView.setRating(Float.parseFloat(request.getRate()));
        }

        binding.layanan.setText(pelanggan.getFullnama());
        binding.pickUpText.setText(request.getAlamatAsal());
        binding.destinationText.setText(request.getAlamatTujuan());

        if (request.getHome().equals("4")) {
            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);
        } else {
            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), this);
        }

    }
}
