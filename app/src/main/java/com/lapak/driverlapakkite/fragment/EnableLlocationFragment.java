package com.lapak.driverlapakkite.fragment;


import static android.content.Context.MODE_PRIVATE;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.activity.IntroActivity;
import com.lapak.driverlapakkite.activity.MainActivity;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.models.User;


public class EnableLlocationFragment extends Fragment {

    View getView;
    Context context;
    Button enableLocation;
    SharedPreferences sharedPreferences;

    ActivityResultLauncher<String[]> requestPermission;
    ActivityResultLauncher<Intent> startResult;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getView = inflater.inflate(R.layout.fragment_enablelocation, container, false);
        context = getContext();

        enableLocation = getView.findViewById(R.id.enable_location_btn);
        return getView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = context.getSharedPreferences(Constant.PREF_NAME, MODE_PRIVATE);

        requestPermission = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
            if (result.containsValue(true)) {
                GetCurrentlocation();
            }
        });

        startResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            GPSStatus();
        });

        enableLocation.setOnClickListener(v -> getLocationPermission());
    }

    private void getLocationPermission() {
        requestPermission.launch(new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
    }


    public void GPSStatus() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!GpsStatus) {
            Toast.makeText(context, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startResult.launch(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        } else {
            GetCurrentlocation();
        }
    }


    private void GetCurrentlocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            return;
        }
        GoToNext_Activty();
    }


    public void GoToNext_Activty() {
        final User user = BaseApp.getInstance(context).getLoginUser();
        if (user != null) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
            requireActivity().finish();
        } else {
            Intent intent = new Intent(getActivity(), IntroActivity.class);
            startActivity(intent);
            requireActivity().finish();
        }

    }


}

