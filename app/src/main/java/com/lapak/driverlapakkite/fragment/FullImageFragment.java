package com.lapak.driverlapakkite.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;
import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FullImageFragment extends Fragment {

    private Context context;

    private String chat_id;
    private ProgressBar progressBar;

    private ProgressDialog progressDialog;

    FirebaseStorage storage;
    StorageReference storageReference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View getView = inflater.inflate(R.layout.fragment_fullimage, container, false);
        context = getContext();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        String imageUrl = requireArguments().getString("image_url");
        chat_id = requireArguments().getString("chat_id");

        ImageView closeGallery = getView.findViewById(R.id.close_gallery);
        closeGallery.setOnClickListener(v -> requireActivity().onBackPressed());

        progressDialog = new ProgressDialog(context, R.style.DialogStyle);
        progressDialog.setMessage("Please Wait");

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        Button savebtn = getView.findViewById(R.id.savebtn);

        savebtn.setOnClickListener(v -> preparingDownload());
        progressBar = getView.findViewById(R.id.progress);
        ImageView singleImage = getView.findViewById(R.id.single_image);


        progressBar.setVisibility(View.VISIBLE);
        PicassoTrustAll.getInstance(context).load(imageUrl).placeholder(R.drawable.image_placeholder)
                .into(singleImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        progressBar.setVisibility(View.GONE);
                    }

                });


        ImageButton sharebtn = getView.findViewById(R.id.sharebtn);
        sharebtn.setOnClickListener(v ->

                SharePicture(imageUrl));

        return getView;
    }

    private void SharePicture(String imageUrl) {
        Picasso.get().load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {

        Uri bmpUri = null;
        try {
            File file = new File(context.getFilesDir().getAbsolutePath(), "share_image_" + chat_id + ".jpg");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            bmpUri = FileProvider.getUriForFile(
                    context,
                    "com.lapak.driverlapakkite.fileprovider",
                    file);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void preparingDownload() {
        progressDialog.show();
        ExecutorService executorService = Executors.newSingleThreadExecutor();


        storageReference.child("images").child(chat_id + ".jpg").getStream().continueWith(executorService, (Continuation<StreamDownloadTask.TaskSnapshot, Object>) task -> {
            InputStream inputStream = task.getResult().getStream();
            saveFile(inputStream);
            return null;
        }).addOnSuccessListener((OnSuccessListener<Object>) o -> {
            new AlertDialog.Builder(context, R.style.DialogStyle)
                    .setTitle("Foto Tersimpan")
                    .setMessage("Folder Download")
                    .setNegativeButton("ok", (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
            progressDialog.dismiss();
        });
    }

    void saveFile(InputStream inputStream) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, chat_id);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "jpg");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);

            ContentResolver resolver = context.getContentResolver();
            Uri uri = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);


            if (uri != null) {
                try {
                    OutputStream outputStream = resolver.openOutputStream(uri);
                    byte[] buffer = new byte[1024];

                    while (true) {
                        int bytes = inputStream.read(buffer);
                        if (bytes == -1) {
                            outputStream.close();
                            break;
                        }
                        outputStream.write(buffer, 0, bytes);
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } else {
            File target = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    chat_id + ".jpg"
            );
            try {
                FileOutputStream output = new FileOutputStream(target);
                byte[] buffer = new byte[1024];
                while (true) {
                    int bytes = inputStream.read(buffer);
                    if (bytes == -1) {
                        output.close();
                        break;
                    }
                    output.write(buffer, 0, bytes);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}


