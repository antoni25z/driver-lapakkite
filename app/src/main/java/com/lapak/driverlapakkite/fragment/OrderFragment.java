package com.lapak.driverlapakkite.fragment;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.expressions.Expression.color;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lineProgress;
import static com.mapbox.mapboxsdk.style.expressions.Expression.linear;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.match;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineGradient;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.lapak.driverlapakkite.databinding.ActivityDetailOrderBinding;
import com.lapak.driverlapakkite.json.fcm.DefaultResponseJson;
import com.lapak.driverlapakkite.utils.api.service.NotificationService;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.location.OnLocationCameraTransitionListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;
import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.activity.ChatActivity;
import com.lapak.driverlapakkite.activity.MainActivity;
import com.lapak.driverlapakkite.constants.BaseApp;
import com.lapak.driverlapakkite.constants.Constant;
import com.lapak.driverlapakkite.item.ItemPesananItem;
import com.lapak.driverlapakkite.json.AcceptRequestJson;
import com.lapak.driverlapakkite.json.AcceptResponseJson;
import com.lapak.driverlapakkite.json.DetailRequestJson;
import com.lapak.driverlapakkite.json.DetailTransResponseJson;
import com.lapak.driverlapakkite.json.ResponseJson;
import com.lapak.driverlapakkite.json.VerifyRequestJson;
import com.lapak.driverlapakkite.models.CustomerModel;
import com.lapak.driverlapakkite.models.OrderFCM;
import com.lapak.driverlapakkite.models.TransModel;
import com.lapak.driverlapakkite.models.User;
import com.lapak.driverlapakkite.utils.Log;
import com.lapak.driverlapakkite.utils.PicassoTrustAll;
import com.lapak.driverlapakkite.utils.Utility;
import com.lapak.driverlapakkite.utils.api.ServiceGenerator;
import com.lapak.driverlapakkite.utils.api.service.DriverService;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

@SuppressLint("NonConstantResourceId")
public class OrderFragment extends Fragment implements OnMapReadyCallback, OnCameraTrackingChangedListener {

    private Context context;
    private static final int REQUEST_PERMISSION_CALL = 992;
    private String idtrans;
    private String idpelanggan;
    private String response;
    private String onsubmit;

    private ItemPesananItem itemPesananItem;
    private String type;
    boolean gps;

    ActivityDetailOrderBinding binding;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Mapbox.getInstance(requireActivity(), getString(R.string.mapbox_access_token));
        binding = ActivityDetailOrderBinding.inflate(Objects.requireNonNull(inflater), container, false);
        context = getContext();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GPSStatus();

        BottomSheetBehavior<LinearLayout> behavior = BottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        type = "0";
        Bundle bundle = getArguments();
        if (bundle != null) {
            idpelanggan = bundle.getString("customer_id");
            idtrans = bundle.getString("transaction_id");
            response = bundle.getString("response");
        }
        shimmerload();
        switch (response) {
            case "2":
                onsubmit = "2";
                binding.llchat.setVisibility(View.VISIBLE);
                break;
            case "3":
                onsubmit = "3";
                binding.llchat.setVisibility(View.VISIBLE);
                binding.order.setVisibility(View.VISIBLE);
                binding.verifycation.setVisibility(View.GONE);
                binding.order.setText("finish");
                break;
            case "4":
                binding.llchat.setVisibility(View.GONE);
                binding.order.setVisibility(View.GONE);
                binding.layanandes.setText(getString(R.string.notification_finish));
                break;
            case "5":
                binding.llchat.setVisibility(View.GONE);
                binding.layanandes.setText(getString(R.string.notification_cancel));
                break;
        }

        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.getMapAsync(this);
        binding.merchantnear.setHasFixedSize(true);
        binding.merchantnear.setNestedScrollingEnabled(false);
        binding.merchantnear.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        binding.rlprogress.setVisibility(View.GONE);
        binding.textprogress.setText(getString(R.string.waiting_pleaseWait));
    }

    private Point pickup, destination;

    private void getData(final String idtrans, final String idpelanggan, Style style, MapboxMap mapboxMap) {
        final User loginUser = BaseApp.getInstance(context).getLoginUser();
        DriverService services = ServiceGenerator.createService(DriverService.class, loginUser.getEmail(), loginUser.getPassword());
        DetailRequestJson param = new DetailRequestJson();
        param.setId(idtrans);
        param.setIdPelanggan(idpelanggan);
        services.detailtrans(param).enqueue(new Callback<DetailTransResponseJson>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<DetailTransResponseJson> call, @NonNull Response<DetailTransResponseJson> responsedata) {
                if (responsedata.isSuccessful()) {
                    shimmertutup();
                    Log.e("", String.valueOf(Objects.requireNonNull(responsedata.body()).getData().get(0)));
                    final TransModel transaksi = responsedata.body().getData().get(0);
                    final CustomerModel pelanggan = responsedata.body().getPelanggan().get(0);
                    type = transaksi.getHome();

                    long totalBiaya = (Long.parseLong(transaksi.final_cost) + transaksi.getAdminFee());
                    Log.d("2504", transaksi.final_cost);

                    if (transaksi.isPakaiWallet()) {
                        Utility.currencyTXT(binding.incash, "0", requireActivity());
                        Utility.currencyTXT(binding.inwallet, String.valueOf(totalBiaya), requireActivity());
                    } else {
                        Utility.currencyTXT(binding.inwallet, transaksi.getKreditPromo(), requireActivity());
                        Utility.currencyTXT(binding.incash, String.valueOf(totalBiaya), requireActivity());
                    }
                    String formatdistance = String.format(Locale.US, "%.1f", transaksi.getJarak());
                    binding.distance.setText(formatdistance);
                    binding.service.setText(transaksi.getEstimasi());

                    if (onsubmit.equals("2")) {
                        destination = Point.fromLngLat(transaksi.getStartLongitude(), transaksi.getStartLatitude());
                        pickup = Point.fromLngLat(Objects.requireNonNull(locationComponent.getLastKnownLocation()).getLongitude(), locationComponent.getLastKnownLocation().getLatitude());
                        initSources(style, "origin", "ORI_LINE_SOURCE_ID", "ORI_SOURCE_ID");
                        initLayers(style, "ORI_LINE_SOURCE_ID", "ORI_SOURCE_ID", "O_ROUTE_LAYER_ID", "OICON_LAYER_ID");
                        getRoute(mapboxMap, pickup, destination, "origin", "ORI_LINE_SOURCE_ID", "ORI_SOURCE_ID");
                        if (transaksi.getHome().equals("4")) {
                            binding.layanandes.setText("Go buy orders");
                            binding.order.setText("deliver orders");
                            binding.verifycation.setVisibility(View.VISIBLE);
                        } else {
                            binding.layanandes.setText(getString(R.string.notification_accept));
                        }
                        binding.order.setVisibility(View.VISIBLE);
                        binding.order.setOnClickListener(view -> {
                            if (transaksi.getHome().equals("4")) {

                                if (Objects.requireNonNull(binding.verifycation.getText()).toString().isEmpty()) {
                                    Toast.makeText(context, "Masukan Kode Verifikasi!", Toast.LENGTH_SHORT).show();
                                } else {
                                    SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                    String finalDate = timeFormat.format(transaksi.getWaktuOrder());
                                    binding.rlprogress.setVisibility(View.VISIBLE);
                                    verify(binding.verifycation.getText().toString(), pelanggan, transaksi.getToken_merchant(), transaksi.idtransmerchant, finalDate, style, mapboxMap);
                                }
                            } else {
                                start(pelanggan, transaksi.getToken_merchant(), transaksi.idtransmerchant, String.valueOf(transaksi.getWaktuOrder()), style, mapboxMap);
                            }

                        });
                    } else if (onsubmit.equals("3")) {
                        style.removeLayer("O_ROUTE_LAYER_ID");
                        style.removeSource("ORI_LINE_SOURCE_ID");
                        style.removeImage("ORIGIN_ICON_ID");
                        if (locationComponent.getLastKnownLocation() != null) {
                            destination = Point.fromLngLat(transaksi.getEndLongitude(), transaksi.getEndLatitude());
                            pickup = Point.fromLngLat(Objects.requireNonNull(locationComponent.getLastKnownLocation()).getLongitude(), locationComponent.getLastKnownLocation().getLatitude());
                            initSources(style, "destination", "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID");
                            initLayers(style, "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID", "DROUTE_LAYER_ID", "DICON_LAYER_ID");
                            getRoute(mapboxMap, pickup, destination, "destination", "DEST_LINE_SOURCE_ID", "DEST_SOURCE_ID");
                        }
                        if (transaksi.getHome().equals("4")) {
                            binding.layanandes.setText("deliver orders");
                        } else {
                            binding.layanandes.setText(getString(R.string.notification_start));
                        }

                        binding.verifycation.setVisibility(View.GONE);
                        binding.order.setText("Finish");
                        binding.order.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish(pelanggan, transaksi.merchant_token);
                            }
                        });
                    }

                    switch (transaksi.getHome()) {
                        case "3":
                            binding.lldestination.setVisibility(View.GONE);
                            binding.lldistance.setVisibility(View.GONE);
                            binding.service.setText(transaksi.getEstimasi());
                            break;
                        case "4":
                            binding.orderdetail.setVisibility(View.VISIBLE);
                            binding.merchantdetail.setVisibility(View.VISIBLE);
                            binding.merchantinfo.setVisibility(View.VISIBLE);
                            Utility.currencyTXT(binding.deliveryfee, String.valueOf(transaksi.getHarga() - Long.parseLong(transaksi.getTotal_biaya())), context);
                            Utility.currencyTXT(binding.cost, String.valueOf(transaksi.getTotal_biaya()), context);
                            binding.namamerchant.setText(transaksi.getNama_merchant());
                            itemPesananItem = new ItemPesananItem(responsedata.body().getItem(), R.layout.item_pesanan);
                            binding.merchantnear.setAdapter(itemPesananItem);

                            binding.phonemerchant.setOnClickListener(v -> {
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.DialogStyle);
                                alertDialogBuilder.setTitle("Panggil Pelanggan");
                                alertDialogBuilder.setMessage("Anda ingin menelepon Pelapak (+" + transaksi.getTeleponmerchant() + ")?");
                                alertDialogBuilder.setPositiveButton("Ya",
                                        (arg0, arg1) -> {
                                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                                return;
                                            }

                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                                            callIntent.setData(Uri.parse("tel:+" + transaksi.getTeleponmerchant()));
                                            startActivity(callIntent);
                                        });

                                alertDialogBuilder.setNegativeButton("Tidak", (dialog, which) -> dialog.dismiss());

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();


                            });

                            binding.chatmerchant.setOnClickListener(v -> {
                                Intent intent = new Intent(context, ChatActivity.class);
                                intent.putExtra("senderid", loginUser.getId());
                                intent.putExtra("receiverid", transaksi.getId_merchant());
                                intent.putExtra("tokendriver", loginUser.getToken());
                                intent.putExtra("tokenku", transaksi.getToken_merchant());
                                intent.putExtra("name", transaksi.getNama_merchant());
                                intent.putExtra("pic", Constant.IMAGESMERCHANT + transaksi.getFoto_merchant());
                                startActivity(intent);
                            });

                            break;
                        case "2":
                            binding.senddetail.setVisibility(View.VISIBLE);
                            binding.produk.setText(transaksi.getNamaBarang());
                            binding.sendername.setText(transaksi.namaPengirim);
                            binding.receivername.setText(transaksi.namaPenerima);

                            binding.senderphone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.DialogStyle);
                                    alertDialogBuilder.setTitle("Panggil Pengemudi");
                                    alertDialogBuilder.setMessage("Anda ingin menelepon" + transaksi.getNamaPengirim() + "(" + transaksi.teleponPengirim + ")?");
                                    alertDialogBuilder.setPositiveButton("ya",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                                        return;
                                                    }

                                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                                    callIntent.setData(Uri.parse("tel:" + transaksi.teleponPengirim));
                                                    startActivity(callIntent);
                                                }
                                            });

                                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();


                                }
                            });

                            binding.receiverphone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.DialogStyle);
                                    alertDialogBuilder.setTitle("Call Driver");
                                    alertDialogBuilder.setMessage("You want to call " + transaksi.getNamaPenerima() + "(" + transaksi.teleponPenerima + ")?");
                                    alertDialogBuilder.setPositiveButton("yes",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                                        return;
                                                    }

                                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                                    callIntent.setData(Uri.parse("tel:" + transaksi.teleponPenerima));
                                                    startActivity(callIntent);
                                                }
                                            });

                                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();


                                }
                            });

                            break;
                    }

                    parsedata(transaksi, pelanggan);


                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<DetailTransResponseJson> call, @NonNull Throwable t) {

            }
        });


    }

    private void shimmerload() {
        binding.shimmerlayanan.startShimmer();
        binding.shimmerpickup.startShimmer();
        binding.shimmerdestination.startShimmer();
        binding.shimmerfitur.startShimmer();
        binding.shimmerdistance.startShimmer();
        binding.shimmerprice.startShimmer();
        binding.shimmerincash.startShimmer();
        binding.shimmerinwallet.startShimmer();

        binding.layanan.setVisibility(View.GONE);
        binding.layanandes.setVisibility(View.GONE);
        binding.pickUpText.setVisibility(View.GONE);
        binding.destinationText.setVisibility(View.GONE);
        binding.service.setVisibility(View.GONE);
        binding.price.setVisibility(View.GONE);
        binding.incash.setVisibility(View.GONE);
        binding.inwallet.setVisibility(View.GONE);
    }

    private void shimmertutup() {
        binding.shimmerlayanan.stopShimmer();
        binding.shimmerpickup.stopShimmer();
        binding.shimmerdestination.stopShimmer();
        binding.shimmerfitur.stopShimmer();
        binding.shimmerdistance.stopShimmer();
        binding.shimmerprice.stopShimmer();
        binding.shimmerincash.stopShimmer();
        binding.shimmerinwallet.stopShimmer();

        binding.shimmerlayanan.setVisibility(View.GONE);
        binding.shimmerpickup.setVisibility(View.GONE);
        binding.shimmerdestination.setVisibility(View.GONE);
        binding.shimmerfitur.setVisibility(View.GONE);
        binding.shimmerdistance.setVisibility(View.GONE);
        binding.shimmerprice.setVisibility(View.GONE);
        binding.shimmerincash.setVisibility(View.GONE);
        binding.shimmerinwallet.setVisibility(View.GONE);

        binding.layanan.setVisibility(View.VISIBLE);
        binding.layanandes.setVisibility(View.VISIBLE);
        binding.pickUpText.setVisibility(View.VISIBLE);
        binding.destinationText.setVisibility(View.VISIBLE);
        binding.distance.setVisibility(View.VISIBLE);
        binding.service.setVisibility(View.VISIBLE);
        binding.price.setVisibility(View.VISIBLE);
        binding.incash.setVisibility(View.VISIBLE);
        binding.inwallet.setVisibility(View.VISIBLE);
    }

    private void parsedata(TransModel request, final CustomerModel pelanggan) {
        final User loginUser = BaseApp.getInstance(context).getLoginUser();
        binding.rlprogress.setVisibility(View.GONE);

        PicassoTrustAll.getInstance(context)
                .load(Constant.IMAGESUSER + pelanggan.getFoto())
                .placeholder(R.drawable.image_placeholder)
                .into(binding.background);


        binding.layanan.setText(pelanggan.getFullnama());
        binding.pickUpText.setText(request.getAlamatAsal());
        binding.destinationText.setText(request.getAlamatTujuan());

        long totalBiaya = (Long.parseLong(request.final_cost) + request.getAdminFee());
        if (type.equals("4")) {
            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), context);
        } else {
            Utility.currencyTXT(binding.price, String.valueOf(totalBiaya), context);
        }

        binding.phonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.DialogStyle);
                alertDialogBuilder.setTitle("Panggil Pelanggan");
                alertDialogBuilder.setMessage("Anda ingin menelepon Pelapak (+" + pelanggan.getNoTelepon() + ")?");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                    return;
                                }

                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:+" + pelanggan.getNoTelepon()));
                                startActivity(callIntent);
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        binding.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("senderid", loginUser.getId());
                intent.putExtra("receiverid", pelanggan.getId());
                intent.putExtra("tokendriver", loginUser.getToken());
                intent.putExtra("tokenku", pelanggan.getToken());
                intent.putExtra("name", pelanggan.getFullnama());
                intent.putExtra("pic", Constant.IMAGESUSER + pelanggan.getFoto());
                startActivity(intent);
            }
        });
    }


    private Location location;
    @CameraMode.Mode
    private int cameraMode = CameraMode.TRACKING_COMPASS;

    @RenderMode.Mode
    private final int renderMode = RenderMode.NORMAL;

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        if (gps) {
            mapboxMap.setStyle(new Style.Builder().fromUri(Constant.CUSTOM_MAPBOX_STYLE)
                    .withImage("ORIGIN_ICON_ID", Objects.requireNonNull(BitmapUtils.getBitmapFromDrawable(
                            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_pickup_map, null))))
                    .withImage("DESTINATION_ICON_ID", Objects.requireNonNull(BitmapUtils.getBitmapFromDrawable(
                            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_destination_map, null)))), style -> {

                UiSettings uiSettings = mapboxMap.getUiSettings();
                uiSettings.setCompassEnabled(false);
                uiSettings.setAttributionEnabled(false);
                uiSettings.setLogoEnabled(false);

                if (PermissionsManager.areLocationPermissionsGranted(context)) {

                    locationComponent = mapboxMap.getLocationComponent();
                    locationComponent.activateLocationComponent(
                            LocationComponentActivationOptions
                                    .builder(context, style)
                                    .useDefaultLocationEngine(true)
                                    .locationEngineRequest(new LocationEngineRequest.Builder(750)
                                            .setFastestInterval(750)
                                            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                                            .build())
                                    .build());
                    locationComponent.setLocationComponentEnabled(true);
                    setCameraTrackingMode();
                    locationComponent.setCameraMode(cameraMode);
                    locationComponent.setRenderMode(renderMode);
                    locationComponent.forceLocationUpdate(location);
                    binding.currentlocation.setOnClickListener(view -> setCameraTrackingMode());
                    getData(idtrans, idpelanggan, style, mapboxMap);
                } else {

                }

            });
        } else {
            GPSStatus();
        }
    }


    private void initSources(@NonNull Style loadedMapStyle, String iconid, String lineid, String iconsourceid) {

        loadedMapStyle.addSource(new GeoJsonSource(lineid, new GeoJsonOptions().withLineMetrics(true)));
        loadedMapStyle.addSource(new GeoJsonSource(iconsourceid, getOriginAndDestinationFeatureCollection(iconid)));

    }

    private void initLayers(@NonNull Style loadedMapStyle, String lineid, String iconsourceid, String routeid, String iconlayerid) {
        loadedMapStyle.addLayer(new LineLayer(routeid, lineid).withProperties(
                lineCap(Property.LINE_CAP_ROUND),
                lineJoin(Property.LINE_JOIN_ROUND),
                lineWidth(6f),
                lineGradient(interpolate(
                        linear(), lineProgress(),
                        stop(0f, color(context.getResources().getColor(R.color.colorPrimary))),
                        stop(1f, color(context.getResources().getColor(R.color.colorgradient))
                        )))));

        loadedMapStyle.addLayer(new SymbolLayer(iconlayerid, iconsourceid).withProperties(
                iconImage(match(get("originDestination"), literal("origin"),
                        stop("origin", "ORIGIN_ICON_ID"),
                        stop("destination", "DESTINATION_ICON_ID"))),
                iconIgnorePlacement(true),
                iconAllowOverlap(true),
                iconSize(2.0f),
                iconOffset(new Float[]{0f, -4f})));
    }

    MapboxDirections client;
    private DirectionsRoute currentRoute;

    private void getRoute(MapboxMap mapboxMap, Point origin, Point destination, String iconid, String lineid, String iconsourceid) {
        client = MapboxDirections.builder()
                .origin(origin)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_WALKING)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();
        client.enqueueCall(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    return;
                } else if (response.body().routes().size() < 1) {
                    return;
                }

                currentRoute = response.body().routes().get(0);

                if (currentRoute != null) {
                    if (mapboxMap != null) {
                        mapboxMap.getStyle(style -> {
                            GeoJsonSource originDestinationPointGeoJsonSource = style.getSourceAs(iconsourceid);

                            if (originDestinationPointGeoJsonSource != null) {
                                originDestinationPointGeoJsonSource.setGeoJson(getOriginAndDestinationFeatureCollection(iconid));
                            }
                            GeoJsonSource lineLayerRouteGeoJsonSource = style.getSourceAs(lineid);

                            if (lineLayerRouteGeoJsonSource != null) {
                                LineString lineString = LineString.fromPolyline(Objects.requireNonNull(currentRoute.geometry()), PRECISION_6);
                                lineLayerRouteGeoJsonSource.setGeoJson(Feature.fromGeometry(lineString));
                            }
                        });
                    }
                } else {
                    Timber.d("Directions route is null");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
            }
        });
    }

    private FeatureCollection getOriginAndDestinationFeatureCollection(String iconid) {
        Feature destinationFeature = Feature.fromGeometry(destination);
        destinationFeature.addStringProperty("originDestination", iconid);
        return FeatureCollection.fromFeatures(new Feature[]{destinationFeature});
    }


    private void start(final CustomerModel pelanggan, final String tokenmerchant, final String idtransmerchant, final String waktuorder, Style style, MapboxMap mapboxMap) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        final User loginUser = BaseApp.getInstance(context).getLoginUser();
        DriverService userService = ServiceGenerator.createService(
                DriverService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        AcceptRequestJson param = new AcceptRequestJson();
        param.setId(loginUser.getId());
        param.setIdtrans(idtrans);
        userService.startrequest(param).enqueue(new Callback<AcceptResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<AcceptResponseJson> call, @NonNull final Response<AcceptResponseJson> response) {
                if (response.isSuccessful()) {

                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("berhasil")) {
                        binding.rlprogress.setVisibility(View.GONE);
                        onsubmit = "3";
                        getData(idtrans, idpelanggan, style, mapboxMap);
                        OrderFCM.Data orderfcm = new OrderFCM.Data();
                        orderfcm.driver_id = loginUser.getId();
                        orderfcm.transaction_id = idtrans;
                        orderfcm.response = "3";
                        if (type.equals("4")) {
                            orderfcm.customer_id = idpelanggan;
                            orderfcm.invoice = "INV-" + idtrans + idtransmerchant;
                            orderfcm.ordertime = waktuorder;
                            orderfcm.desc = "Pengemudi mengantarkan pesanan";
                            sendMessageToDriver(tokenmerchant, orderfcm);
                        } else {
                            orderfcm.desc = getString(R.string.notification_start);
                        }
                        sendMessageToDriver(pelanggan.getToken(), orderfcm);
                    } else {
                        binding.rlprogress.setVisibility(View.GONE);
                        Intent i = new Intent(context, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        Toast.makeText(context, "Pesanan tidak lagi tersedia!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AcceptResponseJson> call, @NonNull Throwable t) {
                Toast.makeText(context, "Error Connection!", Toast.LENGTH_SHORT).show();
                binding.rlprogress.setVisibility(View.GONE);
            }
        });
    }

    private void verify(String verificode, final CustomerModel pelanggan, final String tokenmerchant, final String idtransmerchant, final String waktuorder, Style style, MapboxMap mapboxMap) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        final User loginUser = BaseApp.getInstance(context).getLoginUser();
        DriverService userService = ServiceGenerator.createService(
                DriverService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        VerifyRequestJson param = new VerifyRequestJson();
        param.setId(loginUser.getNoTelepon());
        param.setIdtrans(idtrans);
        param.setVerifycode(verificode);
        userService.verifycode(param).enqueue(new Callback<ResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<ResponseJson> call, @NonNull final Response<ResponseJson> response) {
                if (response.isSuccessful()) {

                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {

                        start(pelanggan, tokenmerchant, idtransmerchant, waktuorder, style, mapboxMap);
                    } else {
                        binding.rlprogress.setVisibility(View.GONE);
                        Toast.makeText(context, "Verifikasi Kode Salah!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseJson> call, @NonNull Throwable t) {
                Toast.makeText(context, "Error Connection!", Toast.LENGTH_SHORT).show();
                binding.rlprogress.setVisibility(View.GONE);
            }
        });
    }

    private void finish(final CustomerModel pelanggan, final String tokenmerchant) {
        binding.rlprogress.setVisibility(View.VISIBLE);
        final User loginUser = BaseApp.getInstance(context).getLoginUser();
        DriverService userService = ServiceGenerator.createService(
                DriverService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        AcceptRequestJson param = new AcceptRequestJson();
        param.setId(loginUser.getId());
        param.setIdtrans(idtrans);
        userService.finishrequest(param).enqueue(new Callback<AcceptResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<AcceptResponseJson> call, @NonNull final Response<AcceptResponseJson> response) {
                if (response.isSuccessful()) {

                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("berhasil")) {
                        binding.rlprogress.setVisibility(View.GONE);
                        Intent i = new Intent(context, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        OrderFCM.Data orderfcm = new OrderFCM.Data();
                        orderfcm.driver_id = loginUser.getId();
                        orderfcm.transaction_id = idtrans;
                        orderfcm.response = "4";
                        orderfcm.desc = getString(R.string.notification_finish);
                        if (type.equals("4")) {
                            sendMessageToDriver(tokenmerchant, orderfcm);
                            sendMessageToDriver(pelanggan.getToken(), orderfcm);
                        } else {
                            sendMessageToDriver(pelanggan.getToken(), orderfcm);
                        }

                    } else {
                        binding.rlprogress.setVisibility(View.GONE);
                        Intent i = new Intent(context, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        Toast.makeText(context, "Pesanan tidak lagi tersedia!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AcceptResponseJson> call, @NonNull Throwable t) {
                Toast.makeText(context, "Error Connection!", Toast.LENGTH_SHORT).show();
                binding.rlprogress.setVisibility(View.GONE);
            }
        });
    }

    private void sendMessageToDriver(final String regIDTujuan, final OrderFCM.Data response) {

        User loginUser = BaseApp.getInstance(requireContext()).getLoginUser();
        final NotificationService service = ServiceGenerator.createService(NotificationService.class, loginUser.getEmail(), loginUser.getPassword());

        OrderFCM orderFCM = new OrderFCM(regIDTujuan, response);

        service.sendNotification(orderFCM).enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponseJson> call, @NonNull Response<DefaultResponseJson> response) {
                Timber.tag("response_d34").d(Objects.requireNonNull(response.body()).getMessage());
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponseJson> call, @NonNull Throwable t) {
                Timber.tag("response_d34").d(Objects.requireNonNull(t.getMessage()));
            }
        });
    }

    private LocationComponent locationComponent;

    private void setCameraTrackingMode() {
        locationComponent.setCameraMode(CameraMode.TRACKING_COMPASS, new OnLocationCameraTransitionListener() {
            @Override
            public void onLocationCameraTransitionFinished(@CameraMode.Mode int cameraMode) {
                locationComponent.zoomWhileTracking(20, 750, new MapboxMap.CancelableCallback() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onFinish() {
                        locationComponent.tiltWhileTracking(60);
                    }
                });

            }

            @Override
            public void onLocationCameraTransitionCanceled(@CameraMode.Mode int cameraMode) {
            }
        });
    }


    @Override
    public void onCameraTrackingDismissed() {
    }

    @Override
    public void onCameraTrackingChanged(int currentMode) {
        this.cameraMode = currentMode;

    }

    public void GPSStatus() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = Objects.requireNonNull(lm).isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            gps = false;
            Toast.makeText(context, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2);
        } else {
            gps = true;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            GPSStatus();
        }
    }
}
