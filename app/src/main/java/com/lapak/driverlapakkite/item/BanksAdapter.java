package com.lapak.driverlapakkite.item;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.driverlapakkite.databinding.ItemBanksBinding;
import com.wensolution.wensxendit.AvailableBankModel;

import java.util.ArrayList;
import java.util.Locale;

public class BanksAdapter extends RecyclerView.Adapter<BanksAdapter.ViewHolder> implements Filterable {

    ArrayList<AvailableBankModel> availableBankModels;
    ArrayList<AvailableBankModel> availableBankModelsF;

    BanksClick banksClick;

    public BanksAdapter(final ArrayList<AvailableBankModel> availableBankModels, final BanksClick banksClick) {
        this.availableBankModels = availableBankModels;
        availableBankModelsF = new ArrayList(availableBankModels);
        this.banksClick = banksClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBanksBinding binding = ItemBanksBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AvailableBankModel availableBankModel = availableBankModels.get(position);
        holder.binding.bankNameTxt.setText(availableBankModel.getName());

        holder.itemView.setOnClickListener(view -> {
            banksClick.onClick(availableBankModel);
        });
    }

    @Override
    public int getItemCount() {
        return availableBankModels.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<AvailableBankModel> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.toString().isEmpty()) {
                filteredList.addAll(availableBankModelsF);
            } else {
                String filterPatern = charSequence.toString().toLowerCase(Locale.ROOT);
                for (AvailableBankModel item : availableBankModelsF) {
                    if (item.getName().toLowerCase(Locale.ROOT).contains(filterPatern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            availableBankModels.clear();
            availableBankModels.addAll((ArrayList<AvailableBankModel>)filterResults.values);
            notifyDataSetChanged();
        }
    };

    static class ViewHolder extends RecyclerView.ViewHolder {

        ItemBanksBinding binding;

        public ViewHolder(@NonNull ItemBanksBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
