package com.lapak.driverlapakkite.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.driverlapakkite.R;
import com.lapak.driverlapakkite.json.NotificationResponseJson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.ViewHolder> {

    List<NotificationResponseJson.Notification> notificationList;

    public NotifAdapter(final List<NotificationResponseJson.Notification> notificationList) {
        this.notificationList = notificationList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationResponseJson.Notification notification = notificationList.get(position);
        holder.title.setText(notification.getTitle());
        holder.message.setText(notification.getMessage());

        holder.time.setText(convertTimeToText(notification.getId()));
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, message, time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_txt);
            message = itemView.findViewById(R.id.message_txt);
            time = itemView.findViewById(R.id.time_txt);
        }
    }

    public String convertTimeToText(long pushTime) {

        String convTime;

        String suffix = "Lalu";

        Date nowTime = new Date();

        long dateDiff = nowTime.getTime() - pushTime;

        long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
        long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
        long hour   = TimeUnit.MILLISECONDS.toHours(dateDiff);

        if (second < 60) {
            convTime = second + " Detik " + suffix;
        } else if (minute < 60) {
            convTime = minute + " Menit "+suffix;
        } else if (hour < 24) {
            convTime = hour + " Jam "+suffix;
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("id", "ID"));
            convTime = simpleDateFormat.format(pushTime);
        }

        return convTime;
    }

}
