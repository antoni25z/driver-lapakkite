package com.lapak.driverlapakkite.models;

import static com.lapak.driverlapakkite.json.fcm.FCMType.OTHER;

import java.io.Serializable;

/**
 * Created by Ourdevelops Team on 19/10/2019.
 */
public class Notif implements Serializable{
    String token;
    Data data;

    public Notif(final String token, final Data data) {
        this.token = token;
        this.data = data;
    }

    public static class Data {
        public int type = OTHER;
        public String title;
        public String message;
    }
}
